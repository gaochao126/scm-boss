package junit5;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.time.Duration;
import java.time.temporal.ChronoUnit;

@DisplayName("内嵌测试类")
//@SpringBootTest
public class Junit5Test {


    @BeforeAll
    public static void init() {
        System.out.println("ALL初始化数据...");
    }

    @AfterAll
    public static void cleanup() {
        System.out.println("ALL清理数据...");
    }


    @Nested
    @DisplayName("第一个Juint5内嵌单元测试")
    class firstTest {

        @BeforeEach
        public void currStart() {
            System.out.println("CURR当前方法开始...");
        }

        @AfterEach
        public void currEnd() {
            System.out.println("CURR当前方法结束...");
        }

        @DisplayName("第一个测试方法")
        @Test
        void testFirst() {
            System.out.println("执行程序。。。。。。。。。。");
        }

        @DisplayName("第二个测试方法")
        @Disabled
        @Test
        void testDis() {
            System.out.println("过时的方法，跳过....");
        }
    }

    @Nested
    @DisplayName("第二个Juint5内嵌单元测试")
    class secondTest {

        @DisplayName("第二个内嵌测试类第一个方法。。。")
        @Test
        void secondTest() {
            System.out.println("第二个测试类的测试方法。。。。");
        }

        @DisplayName("重复次数测试")
        @Test
        @RepeatedTest(value = 3, name = "{displayName} 第 {currentRepetition} 次")
        void repeatTest() {
            System.out.println("重复次数测试");
        }
    }

    @Nested
    @DisplayName("第三个内嵌测试类")
    class ThreeTest {

        @DisplayName("断言测试")
        @Test
        void assertTest() {
            int[] numbers = {0, 1, 2, 3, 4};
            Assertions.assertAll("numbers"
                    , () -> Assertions.assertEquals(numbers[0], 0)
                    , () -> Assertions.assertEquals(numbers[1], 1)
                    , () -> Assertions.assertEquals(numbers[2], 2)
                    , () -> Assertions.assertEquals(numbers[3], 3, "输入与预期值不符！")
                    , () -> Assertions.assertEquals(numbers[4], 4)
            );

        }

        @Test
        @DisplayName("超时方法测试")
        void test_should_complete_in_one_second() {
            Assertions.assertTimeoutPreemptively(Duration.of(1, ChronoUnit.SECONDS), () -> Thread.sleep(2000));
        }

        @Test
        @DisplayName("测试捕获的异常")
        void assertThrowsException() {
            String str = null;
            Assertions.assertThrows(IllegalArgumentException.class, () -> Integer.valueOf(str));
        }
    }

    @Nested
    @DisplayName("参数化单元测试类")
    class FourTest {

        @DisplayName("参数化单元测试类")
        @ParameterizedTest
        @ValueSource(ints = {2, 4, 5})
        void paramTest(int num) {
            Assertions.assertEquals(0, num % 2);
        }

        @DisplayName("param测试")
        @ParameterizedTest
        @ValueSource(strings = {"Effective Java", "Code Complete", "Clean Code"})
        void testPrintTitle(String title) {
            System.out.println(title);
        }

        @DisplayName("csv测试")
        @ParameterizedTest
        @CsvSource({"1,One", "2,Two", "3,Three"})
        void testDataFromCsv(long id, String name) {
            System.out.printf("id: %d, name: %s", id, name);
        }
    }

}
