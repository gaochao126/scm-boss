package junit5;

import cn.hutool.core.io.FileUtil;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.*;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@DisplayName("20210823测试用例执行")
@Slf4j
@Data
public class JunitTest20210823 {

    private long start = System.currentTimeMillis();


    @DisplayName("前置")
    @BeforeAll
    public static void beforeAll() {
        log.info("前置...");
    }

    @DisplayName("后置")
    @AfterAll
    public static void afterAll() {
        log.info("后置");
    }


    @BeforeEach
    public void beforeEach() {
        log.info("write 开始");
        this.setStart(System.currentTimeMillis());
    }

    @AfterEach
    public void afterEach() {
        log.info("write 结束，耗时：{}", System.currentTimeMillis() - this.getStart());

    }

    @Test
    @DisplayName("像硬盘写入txt文件，500w数据")
    public void writeTxt() {
        File file = new File("e:\\T.txt");
        List<String> list = new ArrayList<>();
        for (int i = 0; i <= 500; i++) {
            String id = String.format("07%d", i);
            String sex = i % 2 == 0 ? "0" : "1";
            String str = "张三" + id + "," + sex + "," + "重庆市渝北区洪湖东路"+id;
            list.add(str);

            if (list.size() == 500) {
                log.info("当前条数：{}，写入文件一次：", i);
                FileUtil.writeLines(list, file, "UTF-8", true);
                list = new ArrayList<>();
            }
        }

    }
}
