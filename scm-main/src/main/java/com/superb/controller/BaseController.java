package com.superb.controller;

import com.superb.construct.ConstructFactory;
import com.superb.controller.aspect.SysLog;
import com.superb.model.UserEntity;
import com.superb.user.UserService;
import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.SqlSessionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "baseController")
@Slf4j
public class BaseController {

    @Autowired
    UserService userService;

    @Autowired
    ConstructFactory constructFactory;

    @Autowired
    SqlSessionTemplate sqlSessionTemplate;

    @Autowired
    JdbcTemplate jdbcTemplate;

    @SysLog(value = "登录方法测试啊。。")
    @GetMapping(value = "login")
    public String login(@RequestParam("sex") int sex) {

        List<UserEntity> userBySex = userService.listUser();

        System.out.println(userBySex.toString());

        new Thread(() -> {
        });

        return userBySex.toString();
    }

    @GetMapping(value = "print")
    public String print() {
        return "哈哈哈 print";
    }

    @GetMapping(value = "getByRedis")
    public String getByRedis() {
        userService.initUserToRedis();

        String userFromRedis = userService.getUserFromRedis();
        return userFromRedis;
    }


    @GetMapping(value = "getService")
    public String getService() {
        return constructFactory.getService("a").send("哈哈哈哈");
    }

    @GetMapping(value = "query")
    public String query() throws Exception {
        String sql = "INSERT INTO `user` (`user_name`, `user_sex`, `user_address`) VALUES ('动态添加', '1', '重庆市渝北区洪湖东路07499')";

        Connection connection = SqlSessionUtils.getSqlSession(
                this.sqlSessionTemplate.getSqlSessionFactory(), this.sqlSessionTemplate.getExecutorType(),
                this.sqlSessionTemplate.getPersistenceExceptionTranslator()).getConnection();

        connection.createStatement().execute(sql);

        return "添加完成";
    }

    @GetMapping(value = "jdbcTemp")
    public String jdbcTemp() {
        String sql = "INSERT INTO `user` (`user_name`, `user_sex`, `user_address`) VALUES ('动态添加222', '1', '重庆市渝北区洪湖东路07499')";

        jdbcTemplate.execute(sql);

        return "jdbcTemplate添加完成";
    }

    @GetMapping(value = "batchJdbcTemp")
    public String batchJdbcTemp() {

        List<Object[]> list = new ArrayList<>();
        String sql = "INSERT INTO `user` (`user_name`, `user_sex`, `user_address`) VALUES (?, ?, ?)";

        String[] s1 = {"批量插入1", "1","啊哈哈哈"};
        String[] s2 = {"批量插入2", "0","啊哈哈哈"};
        String[] s3 = {"批量插入3", "1","啊哈哈哈"};

        list.add(s1);
        list.add(s2);
        list.add(s3);
        jdbcTemplate.batchUpdate(sql, list);

        return "batchJdbcTemplate添加完成";
    }
}
