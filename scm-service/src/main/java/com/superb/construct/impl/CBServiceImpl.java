package com.superb.construct.impl;

import com.superb.construct.ConstructService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class CBServiceImpl implements ConstructService {
    @Override
    public String send(String name) {
        return "bbbbbbbb：" + name;
    }

    @Override
    public String getKey() {
        return "b";
    }
}
