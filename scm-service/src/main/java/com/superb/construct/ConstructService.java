package com.superb.construct;

public interface ConstructService {

    String send(String name);

    String getKey();
}
