package com.superb.construct;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 工厂方法，构造方法注入，将同类型server实现类，默认注入到map中
 */
@Component
@Slf4j
public class ConstructFactory {

    private Map<String, ConstructService> map = new HashMap<>();

    public ConstructFactory(List<ConstructService> serviceList) {
        log.info("有参构造方法。。。。。。。。。。");
        serviceList.forEach(e -> {
            map.put(e.getKey(), e);
        });
    }

    public ConstructService getService(String key) {
        return map.get(key);
    }
}
