package com.superb.springbatch.config;

import com.alibaba.druid.pool.DruidDataSource;
import com.superb.mapper.UserMapper;
import com.superb.model.UserEntity;
import com.superb.springbatch.listener.JobListener;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.PathResource;

import javax.annotation.Resource;
import javax.sql.DataSource;


@Configuration
@Slf4j
public class DataBatchHandleConfig {

    @Resource
    JobBuilderFactory jobBuilderFactory;

    @Resource
    StepBuilderFactory stepBuilderFactory;

    @Resource
    DruidDataSource dataSource;

    @Resource
    JobListener jobListener;


    // 配置一个ItemReader，即数据的读取逻辑
    @Bean
    @StepScope
    FlatFileItemReader<UserEntity> itemReader() {
        // FlatFileItemReader 是一个加载普通文件的 ItemReader
        FlatFileItemReader<UserEntity> reader = new FlatFileItemReader<>();
        // 由于data.csv文件第一行是标题，因此通过setLinesToSkip方法设置跳过一行
        reader.setLinesToSkip(2);
        // setResource方法配置data.csv文件的位置
        reader.setResource(new PathResource("E:\\T.txt"));
        // 通过setLineMapper方法设置每一行的数据信息
        reader.setLineMapper(new DefaultLineMapper<UserEntity>() {{
            setLineTokenizer(new DelimitedLineTokenizer() {{
                // setNames方法配置了data.csv文件一共有4列，分别是id、username、以及sex,
                setNames("userName", "userSex", "userAddress");
                // 配置列与列之间的间隔符（这里是空格）
                setDelimiter(",");
            }});
            setFieldSetMapper(new BeanWrapperFieldSetMapper() {{
                setTargetType(UserEntity.class);
            }});
        }});
        return reader;
    }


    @Bean
    public ItemProcessor<UserEntity, UserEntity> getDataProcessor() {
        return access -> {
            log.info("processor data : " + access.toString());  //模拟  假装处理数据,这里处理就是打印一下
            return access;
        };

    }


    // 配置ItemWriter，即数据的写出逻辑
    @Bean
    JdbcBatchItemWriter jdbcBatchItemWriter() {
        log.info("userName:{},userSex:{},userAddress:{}");
        // 使用的JdbcBatchltemWriter则是通过JDBC将数据写出到一个关系型数据库中。
        JdbcBatchItemWriter writer = new JdbcBatchItemWriter();
        // 配置使用的数据源
        writer.setDataSource(dataSource);
        // 配置数据插入SQL，注意占位符的写法是":属性名"
        writer.setSql("insert into user(user_name,user_sex,user_address) " +
                "values(:userName,:userSex,:userAddress)");

        // 最后通过BeanPropertyItemSqlParameterSourceProvider实例将实体类的属性和SQL中的占位符一一映射
        writer.setItemSqlParameterSourceProvider(
                new BeanPropertyItemSqlParameterSourceProvider<>());
        return writer;
    }


    // 配置一个Step
    @Bean
    Step csvStep() {
        // Step通过stepBuilderFactory进行配置
        return stepBuilderFactory.get("csvStep") //通过get获取一个StepBuilder，参数数Step的name
                .<UserEntity, UserEntity>chunk(2) //方法的参数2，表示每读取到两条数据就执行一次write操作
                .reader(itemReader()) // 配置reader
                .processor(getDataProcessor())
                .writer(jdbcBatchItemWriter()) // 配置writer
                .build();
    }

    // 配置一个Job
    @Bean
    Job csvJob() {
        // 通过jobBuilderFactory构建一个Job，get方法参数为Job的name
        return jobBuilderFactory.get("csvJob")
                .start(csvStep()) // 配置该Job的Step
                .listener(jobListener)
                .build();
    }
}
