package com.superb.design.listener;

// 监听器
// 事件三要素：事件源-source，时间-when，主题-message
// 事件模型构成：事件-event，事件源-source，监听器-listener
// 时间流转过程：事件源注册监听器--->>事件发生--->>通知监听器--->>事件处理
//