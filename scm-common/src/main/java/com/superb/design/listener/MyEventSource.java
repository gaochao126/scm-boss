package com.superb.design.listener;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 事件源实现
 */
@Slf4j
public class MyEventSource implements EventSource{
    /**
     * 监听器集合
     */
    List<EventListener> listeners = new ArrayList<>();

    /**
     * 触发因子，满足指定条件后，监听器触发
     */
    private int value;


    @Override
    public void addEventListener(EventListener eventListener) {
        this.listeners.add(eventListener);
    }

    @Override
    public void notifyListener() {
        for(EventListener listener : listeners){
            MyEvent event = new MyEvent();
            event.setSource(this);
            event.setMessage("set value :" + value);
            event.setWhen(new Date());
            listener.handleEvent(event);
        }
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
        if(value==50){
            log.info("条件满足，触发监听器....");
            notifyListener();
        }else{
            log.info("条件不满足，没有触发监听器....");
        }
    }
}
