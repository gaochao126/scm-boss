package com.superb.design.listener;

/**
 * 事件监听器实现
 */
public class MyEventListener implements EventListener {
    @Override
    public void handleEvent(Event event) {
        event.callback();
    }
}
