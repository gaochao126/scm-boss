package com.superb.design.listener;

/**
 * 时间监听器接口
 */
public interface EventListener {

    /**
     * 时间触发
     * @param event 事件载体
     */
    void handleEvent(Event event);
}
