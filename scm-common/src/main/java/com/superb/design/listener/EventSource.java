package com.superb.design.listener;

/**
 * 事件源
 */
public interface EventSource {

    /**
     * 添加事件监听器
     *
     * @param eventListener
     */
    void addEventListener(EventListener eventListener);

    /**
     * 唤醒监听器
     */
    void notifyListener();
}
