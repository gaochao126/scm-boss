package com.superb.design.listener;

import java.io.Serializable;
import java.util.Date;

/**
 * 事件载体
 */
public interface Event extends Serializable {

    Object getSource();

    Date getWhen();

    String getMessage();

    void callback();
}
