package com.superb.design.listener;

import lombok.extern.slf4j.Slf4j;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 事件实现
 */
@Slf4j
public class MyEvent implements Event {

    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

    private Object source;

    private Date when;

    private String message;


    @Override
    public Object getSource() {
        return source;
    }

    public void setSource(Object source) {
        this.source = source;
    }

    @Override
    public Date getWhen() {
        return when;
    }

    public void setWhen(Date when) {
        this.when = when;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public void callback() {
        log.info(toString());
    }

    @Override
    public String toString() {
        return "回调方法：MyEvent{" +
                "source=" + source +
                ", when=" + when +
                ", message='" + message + '\'' +
                '}';
    }
}
