package com.superb.design.listener;

public class ListenerMain {

    public static void main(String[] args) {
        testEventListener();
    }

    public static void testEventListener() {
        MyEventSource source = new MyEventSource();
        source.addEventListener(new MyEventListener());
        source.setValue(50);
    }
}
